import axios from "axios";
import { base_url } from "../../utils/base_url";
import { config } from "../../utils/axiosconfig";

const getCoupons = async () => {
  try {
    const response = await axios.get(`${base_url}coupon/all-coupon`,config);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching coupon: " + error.message);
  }
};

const createCoupon = async (coupon) => {
  try {
    const response = await axios.post(`${base_url}coupon/register`, coupon, config);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching coupons: " + error.message);
  }
};


const couponService = {
  getCoupons,
  createCoupon
};

export default couponService;
