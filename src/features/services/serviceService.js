import axios from "axios";
import { base_url } from "../../utils/base_url";
import { config } from "../../utils/axiosconfig";

const getServices = async () => {
  try {
    const response = await axios.get(`${base_url}service/all-service`);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching service: " + error.message);
  }
};

const createService = async (service) => {
  try {
    const response = await axios.post(`${base_url}service/register`, service, config);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching services: " + error.message);
  }
};
const ServiceService = {
  getServices,
  createService
};

export default ServiceService;
