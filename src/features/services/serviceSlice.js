import { createAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import ServiceService from "./serviceService";

export const getServices = createAsyncThunk(
  "services/get-services",
  async (thunkAPI) => {
    try {
      return await ServiceService.getServices();
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const createService = createAsyncThunk(
  "service/create-services",
  async (serviceData) => {
    try {
      return await ServiceService.createService(serviceData);
    } catch (error) {
      throw new Error(error);
    }
  }
);

export const resetState = createAction("Reset_all");

const initialState = {
  services: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: "",
  createdService: null,
};

export const serviceSlice = createSlice({
  name: "services",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getServices.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getServices.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.services = action.payload;
      })
      .addCase(getServices.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      })
      .addCase(createService.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createService.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.createdService = action.payload;
      })
      .addCase(createService.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      })
      .addCase(resetState, () => initialState);
  },
});

export default serviceSlice.reducer;
