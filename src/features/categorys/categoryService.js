import axios from "axios";
import { base_url } from "../../utils/base_url";
import { config } from "../../utils/axiosconfig";

const getCategorys = async () => {
  try {
    const response = await axios.get(`${base_url}category/all-category`);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching category: " + error.message);
  }
};

const createCategory = async (category) => {
  try {
    const response = await axios.post(`${base_url}category/register`, category, config);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching categorys: " + error.message);
  }
};

const categoryService = {
  getCategorys,
  createCategory
};

export default categoryService;
