import axios from "axios";
import { base_url } from "../../utils/base_url";

import { config } from "../../utils/axiosconfig";

// const getTokenFromLocalStorage = localStorage.getItem("user")
//   ? JSON.parse(localStorage.getItem("user"))
//   : null;

// const config = {
//   headers: {
//     Authorization: `Bearer ${getTokenFromLocalStorage.token}`,
//     Accept: "Application/json",
//   },
// };

const login = async (userData) => {
  const response = await axios.post(`${base_url}user/admin-login`, userData);
  if (response.data) {
    localStorage.setItem("user", JSON.stringify(response.data));
  }
  return response.data;
};

const getOrders = async () => {
  try {
    const response = await axios.get(`${base_url}user/getallorders`, config);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching order: " + error.message);
  }
};

const authService = {
  login,
  getOrders,
};

export default authService;
