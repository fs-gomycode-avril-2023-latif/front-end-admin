import axios from "axios";
import { base_url } from "../../utils/base_url";
import { config } from "../../utils/axiosconfig";

const getBrands = async () => {
  try {
    const response = await axios.get(`${base_url}brand/all-brand`);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching brand: " + error.message);
  }
};

const getaBrand = async (id) => {
  try {
    const response = await axios.get(`${base_url}brand/${id}`, config);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching brands: " + error.message);
  }
};

const createBrand = async (brand) => {
  try {
    const response = await axios.post(`${base_url}brand/register`, brand, config);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching brands: " + error.message);
  }
};

const brandService = {
  getBrands,
  createBrand,
  getaBrand
};

export default brandService;
