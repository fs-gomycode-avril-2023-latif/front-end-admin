import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import personnelService from "./personnelService";

export const getUsers = createAsyncThunk("personnels/get-users", async (thunkAPI) => {
  try {
    return await personnelService.getUsers();
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const createUser = createAsyncThunk(
  "user/create-users",
  async (userData) => {
    try {
      return await personnelService.createUser(userData);
    } catch (error) {
      throw new Error(error);
    }
  }
);



const initialState = {
  personnels: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: "",
  createdUser : null
};

export const personnelSlice = createSlice({
  name: "personnels",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getUsers.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getUsers.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.personnels = action.payload;
      })
      .addCase(getUsers.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      })
      .addCase(createUser.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createUser.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.createdUser = action.payload;
      })
      .addCase(createUser.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      });
  },
});

export default personnelSlice.reducer;
