import axios from "axios";
import { base_url } from "../../utils/base_url";
import { config } from "../../utils/axiosconfig";

const getUsers = async () => {
  const response = await axios.get(`${base_url}user/all-users`);
  return response.data;
};

const createUser = async (user) => {
  try {
    const response = await axios.post(`${base_url}user/register`, user, config);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching users: " + error.message);
  }
};

const personnelService = {
  getUsers,
  createUser
};

export default personnelService;
