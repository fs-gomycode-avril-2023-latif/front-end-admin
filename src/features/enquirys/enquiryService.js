import axios from "axios";
import { base_url } from "../../utils/base_url";

const getEnquirys = async () => {
  try {
    const response = await axios.get(`${base_url}enquiry/all-enquiry`);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching enquiry: " + error.message);
  }
};

const enquiryService = {
  getEnquirys,
};

export default enquiryService;
