import { createAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import enquiryService from "./enquiryService";

export const getEnquirys = createAsyncThunk(
  "enquirys/get-enquirys",
  async (thunkAPI) => {
    try {
      return await enquiryService.getEnquirys();
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const resetState = createAction("Reset_all")

const initialState = {
  enquirys: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  message: "",
};

export const enquirySlice = createSlice({
  name: "brands",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getEnquirys.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getEnquirys.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.enquirys = action.payload;
      })
      .addCase(getEnquirys.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.message = action.error;
      })
      .addCase(resetState,()=> initialState)
  },
});

export default enquirySlice.reducer;
