import { createAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import roomService from "./roomService";

export const getRooms = createAsyncThunk(
  "rooms/get-rooms",
  async () => {
    try {
      return await roomService.getRooms();
    } catch (error) {
      throw new Error(error);
    }
  }
);

export const createRoom = createAsyncThunk(
  "room/create-rooms",
  async (roomData) => {
    try {
      return await roomService.createRoom(roomData);
    } catch (error) {
      throw new Error(error);
    }
  }
);

export const resetState = createAction("Reset_all")

const initialState = {
  rooms: [],
  isLoading: false,
  isError: false,
  isSuccess: false,
  errorMessage: "",
  createdRoom: null, // Chambre créée récemment
};

export const roomSlice = createSlice({
  name: "rooms",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getRooms.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getRooms.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.rooms = action.payload;
      })
      .addCase(getRooms.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.errorMessage = action.error.message || "Une erreur s'est produite lors de la récupération des chambres.";
      })
      .addCase(createRoom.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createRoom.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isError = false;
        state.isSuccess = true;
        state.createdRoom = action.payload; // Mettez à jour la chambre créée récemment
        state.rooms.push(action.payload);
      })
      .addCase(createRoom.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.isSuccess = false;
        state.errorMessage = action.error.message || "Une erreur s'est produite lors de la création de la chambre.";
      })
      .addCase(resetState,()=> initialState)
  },
});

export default roomSlice.reducer;
