import axios from "axios";
import { base_url } from "../../utils/base_url";
import { config } from "../../utils/axiosconfig";

const getRooms = async () => {
  try {
    const response = await axios.get(`${base_url}room/all-room`);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching rooms: " + error.message);
  }
};

const createRoom = async (room) => {
  try {
    const response = await axios.post(`${base_url}room/register`, room, config);
    return response.data;
  } catch (error) {
    throw new Error("An error occurred while fetching rooms: " + error.message);
  }
};

const roomService = {
  getRooms,
  createRoom
};

export default roomService;
