import React, { useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import InputComponent from "../../components/InputComponent";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { createCategory, resetState } from "../../features/categorys/categorySlice";
import { toast } from "react-toastify";

let userSchema = Yup.object().shape({
  nom: Yup.string().required("ce champ doit etre rempli"),
});

const AddCategory = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const newCategory = useSelector((state) => state.category);
  const { isLoading, isError, isSuccess, createdCategory } = newCategory;

  useEffect(() => {
    if (isSuccess && createdCategory) {
      toast.success("Nouvelle categorie ajoute avec sucess");
    }
    if (isError) {
      toast.error("Echec lors de l'ajout de la nouvelle categorie");
    }
  }, [isLoading, isError, isSuccess, createdCategory]);

  const formik = useFormik({
    initialValues: {
      nom: "",
    },
    validationSchema: userSchema,

    onSubmit: async (values) => {
      // alert(JSON.stringify(values))
      dispatch(createCategory(values));
      formik.resetForm();
      setTimeout(() => {
        dispatch(resetState())
        navigate("/admin/list-categorys");
      }, 3000);
      try {
      } catch (error) {
        // Gérer les erreurs
        throw new Error(error);
      }
    },
  });
  return (
    <form className="row g-3" onSubmit={formik.handleSubmit}>
      <h4 className="text-center mb-4 fw-bold">
        Formulaire d'ajout d'une categorie
      </h4>
      <div className="col-md-12">
        <InputComponent
          type="text"
          name="nom"
          placeholder="Nom de la categorie"
          onch={formik.handleChange("nom")}
          onblr={formik.handleBlur("nom")}
          val={formik.values.nom}
        />
        <div className="text-danger">
          {formik.touched.nom && formik.errors.nom ? (
            <div>{formik.errors.nom}</div>
          ) : null}
        </div>
      </div>

      <button type="submit" className="btn btn-lg btn-primary fs-5">
        Envoyer
      </button>
    </form>
  );
};

export default AddCategory;
