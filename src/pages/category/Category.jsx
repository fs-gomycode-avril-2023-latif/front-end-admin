import React, { useEffect } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { AiFillEdit } from "react-icons/ai";
import { AiFillDelete } from "react-icons/ai";
import { getCategorys } from "../../features/categorys/categorySlice";

const columns = [
  {
    title: "Sno",
    dataIndex: "key",
  },
  {
    title: "Name",
    dataIndex: "name",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.name.length - b.name.length,
  },
  {
    title: "Action",
    dataIndex: "action",
  },
];

const Category = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCategorys());
  }, []);

  const categoryState = useSelector((state) => state.category.categorys);
  const data1 = [];

  for (let i = 0; i < categoryState.length; i++) {
    data1.push({
      key: i + 1,
      name: categoryState[i].nom,
      action: (
        <>
          <Link to={`/admin/coupon/${categoryState[i]._id}`} className="text-decoration-none text-warning pe-2">
            <AiFillEdit className="fs-5 " /> Modifier
          </Link>
          <Link className="text-decoration-none text-danger ">
            <AiFillDelete className="fs-5 " /> Supprimer
          </Link>
        </>
      ),
    });
  }

  return (
    <>
      <h3>La liste des categories</h3>
      <Table columns={columns} dataSource={data1} />;
    </>
  );
};

export default Category;
