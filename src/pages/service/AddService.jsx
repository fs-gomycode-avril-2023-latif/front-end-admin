import React, { useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import InputComponent from "../../components/InputComponent";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { createService, resetState } from "../../features/services/serviceSlice";

let userSchema = Yup.object().shape({
  nom: Yup.string().required("ce champ doit etre rempli"),
  price: Yup.string().required("ce champ doit etre rempli"),
  quantity: Yup.string().required("ce champ doit etre rempli"),
});

const AddService = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const newService = useSelector((state) => state.service);
  const { isLoading, isError, isSuccess, createdService } = newService;

  useEffect(() => {
    if (isSuccess && createdService) {
      toast.success("Nouveau service ajoute avec sucess");
    }
    if (isError) {
      toast.error("Echec lors de l'ajout de la nouveau service");
    }
  }, [isLoading, isError, isSuccess, createdService]);

  const formik = useFormik({
    initialValues: {
      nom: "",
      price: "",
      quantity: "",
    },
    validationSchema: userSchema,

    onSubmit: async (values) => {
      dispatch(createService(values));
      dispatch(resetState())
      formik.resetForm();
      setTimeout(() => {
        navigate("/admin/list-service");
      }, 3000);
      try {
      } catch (error) {
        // Gérer les erreurs
        throw new Error(error);
      }
    },
  });
  return (
    <form className="row g-3" onSubmit={formik.handleSubmit}>
      <h4 className="text-center mb-4 fw-bold">
        Formulaire d'ajout d'un service
      </h4>
      <div className="col-md-12">
        <InputComponent
          type="text"
          name="nom"
          placeholder="Nom de la categorie"
          onch={formik.handleChange("nom")}
          onblr={formik.handleBlur("nom")}
          val={formik.values.nom}
        />
        <div className="text-danger">
          {formik.touched.nom && formik.errors.nom ? (
            <div>{formik.errors.nom}</div>
          ) : null}
        </div>
      </div>
      <div className="col-md-12">
        <InputComponent
          type="text"
          name="price"
          placeholder="Prix du service"
          onch={formik.handleChange("price")}
          onblr={formik.handleBlur("price")}
          val={formik.values.price}
        />
        <div className="text-danger">
          {formik.touched.price && formik.errors.price ? (
            <div>{formik.errors.price}</div>
          ) : null}
        </div>
      </div>
      <div className="col-md-12">
        <InputComponent
          type="number"
          name="quantity"
          placeholder="Quantity disponible"
          min={1}
          max={100}
          onch={formik.handleChange("quantity")}
          onblr={formik.handleBlur("quantity")}
          val={formik.values.quantity}
        />
        <div className="text-danger">
          {formik.touched.quantity && formik.errors.quantity ? (
            <div>{formik.errors.quantity}</div>
          ) : null}
        </div>
      </div>

      <button type="submit" className="btn btn-lg btn-primary fs-5">
        Envoyer
      </button>
    </form>
  );
};

export default AddService;
