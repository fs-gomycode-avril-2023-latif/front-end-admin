import React, { useEffect } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { AiFillEdit } from "react-icons/ai";
import { AiFillDelete } from "react-icons/ai";
import { getCoupons } from "../../features/coupons/couponSlice";
import { getServices } from "../../features/services/serviceSlice";

const columns = [
  {
    title: "Sno",
    dataIndex: "key",
  },
  {
    title: "Name",
    dataIndex: "name",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.name.length - b.name.length,
  },
  {
    title: "Prix",
    dataIndex: "price",
  },
  {
    title: "Quantite",
    dataIndex: "quantity",
  },
  {
    title: "Action",
    dataIndex: "action",
  },
];

const Service = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getServices());
  }, []);

  const serviceState = useSelector((state) => state.service.services);
  const data1 = [];

  for (let i = 0; i < serviceState.length; i++) {
    data1.push({
      key: i + 1,
      name: serviceState[i].nom,
      price: `${serviceState[i].price} Francs CFA` ,
      quantity: serviceState[i].quantity,
      action: (
        <>
          <Link to={`/admin/coupon/${serviceState[i]._id}`} className="text-decoration-none text-warning pe-2">
            <AiFillEdit className="fs-5 " /> Modifier
          </Link>
          <Link className="text-decoration-none text-danger ">
            <AiFillDelete className="fs-5 " /> Supprimer
          </Link>
        </>
      ),
    });
  }

  return (
    <>
      <h3>La liste des services</h3>
      <Table columns={columns} dataSource={data1} />;
    </>
  );
};

export default Service;
