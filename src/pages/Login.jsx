import React, { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { BsShop } from "react-icons/bs";
import { BsFacebook } from "react-icons/bs";
import { BsInstagram } from "react-icons/bs";
import { BsLinkedin } from "react-icons/bs";
import InputComponent from "../components/InputComponent";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../features/auth/authSlice";

let userSchema = Yup.object().shape({
  email: Yup.string()
    .email("email doit etre valide")
    .required("ce champ doit etre rempli"),
  password: Yup.string("enter un mot de passe valide").required(
    "ce champ doit etre rempli"
  ),
});

const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: userSchema,
    onSubmit: (values) => {
      dispatch(login(values));
      // alert(JSON.stringify(values, null, 2));
    },
  });
  const authState = useSelector((state) => state);
  const { user, isLoading, isSuccess, isError, message } = authState.auth;
  useEffect(() => {
    if (isSuccess) {
      navigate("admin");
    } else {
      navigate("");
    }
  }, [user, isLoading, isSuccess, isError, message]);

  return (
    <div
      className="container-fluid d-flex justify-content-center align-items-center"
      id="wrapper"
    >
      <div className="row rounded-5" id="login-wrapper">
        <div className="col-sm-12 col-md-6 p-5" id="detail-entreprise">
          <div className="d-flex align-items-end" id="login-part1">
            <Link to="/" className="text-decoration-none">
              <BsShop className=" text-primary" id="logo" />
            </Link>
            <p className="text-primary ms-2" id="nomEntreprise">
              Nom Logo
            </p>
          </div>
          <div id="login-part2">
            <h2 className="fs-4 fw-bold">WELCOME THE PAGE ENTREPRISE</h2>
            <p>
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Tenetur
              aspernatur expedita adipisci recusandae labore, maiores, molestiae
              quos nisi a doloribus, ab officiis sapiente ratione veniam saepe
              nihil et iste voluptas!
            </p>
            <ul id="icons-social" className="list-style-none">
              <li>
                <Link to="" className="text-decoration-none text-primary">
                  <BsFacebook />
                </Link>
              </li>
              <li>
                <Link to="" className="text-decoration-none text-primary">
                  <BsInstagram />
                </Link>
              </li>
              <li>
                <Link to="" className="text-decoration-none text-primary">
                  <BsLinkedin />
                </Link>
              </li>
            </ul>
          </div>
        </div>
        <div
          className="col-sm-12 col-md-6 p-5 d-flex align-items-center "
          id="form-connect"
        >
          <div className="row align-items-center">
            <div className="header-text text-secondary lead mb-4">
              {/* logo */}
              <div className="d-md-none text-center mb-5">
                <div>
                  <Link to="/" className="text-decoration-none">
                    <BsShop className=" text-primary fs-3" />
                  </Link>
                  <span className="fs-5">Nom Entreprise</span>
                </div>
              </div>
              {/* logo */}
              <div className="d-flex justify-content-between align-items-center ">
                <h2 className="fw-bold text-dark">Sing in</h2>
                <Link to="/sing-up" className="text-decoration-none">
                  <h5>Sing up</h5>
                </Link>
              </div>
              <p>Enter your email and password </p>
            </div>
            <div className=" text-center text-secondary">
              {authState.auth.message ? "You are not admin or password not correcte" : ""}
            </div>
            <form action="" onSubmit={formik.handleSubmit}>
              <InputComponent
                type="text"
                name="email"
                placeholder="Email Address"
                onch={formik.handleChange("email")}
                val={formik.values.email}
              />
              <div className="text-danger">
                {formik.touched.email && formik.errors.email ? (
                  <div>{formik.errors.email}</div>
                ) : null}
              </div>
              <InputComponent
                type="password"
                name="password"
                placeholder="Password"
                onch={formik.handleChange("password")}
                val={formik.values.password}
              />
              <div className="text-danger">
                {formik.touched.password && formik.errors.password ? (
                  <div>{formik.errors.password}</div>
                ) : null}
              </div>
              <div className="input-group mb-3 d-flex justify-content-between mt-2">
                <div className="form-check ">
                  <input
                    type="checkbox"
                    className="form-check-input"
                    id="formCheck"
                  />
                  <label
                    htmlFor="formCheck"
                    className="form-check-label text-secondary "
                  >
                    <small>Remember me</small>
                  </label>
                </div>
                <div className="forgot">
                  <small>
                    <Link
                      to="/forgot-password"
                      className="text-decoration-none text-secondary"
                    >
                      Forgot Password?
                    </Link>
                  </small>
                </div>
              </div>

              <div className="input-group mb-3">
                <button className="text-decoration-none">
                  <button className="btn btn-lg btn-primary fs-5">Login</button>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
