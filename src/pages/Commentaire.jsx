import React from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getBrands } from "../features/brands/brandSlice";
import { Link } from "react-router-dom";
import { AiFillEdit } from "react-icons/ai";
import { getEnquirys } from "../features/enquirys/enquirySlice";

const columns = [
  {
    title: "Key",
    dataIndex: "key",
  },
  {
    title: "Nom",
    dataIndex: "nom",
  },
  {
    title: "Email",
    dataIndex: "email",
  },
  {
    title: "Mobile",
    dataIndex: "mobile",
  },
  {
    title: "Comment",
    dataIndex: "comment",
  },
  {
    title: "Date",
    dataIndex: "date",
  },
  {
    title: "Status",
    dataIndex: "status",
  },
  {
    title: "Actions",
    dataIndex: "action",
  },
];
const Commentaire = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getEnquirys());
  }, []);
  const enquiryState = useSelector((state) => state.enquiry.enquirys);
  const data = [];
  for (let i = 0; i < enquiryState.length; i++) {
    data.push({
      key: i + 1,
      nom: enquiryState[i].nom,
      email: enquiryState[i].email,
      mobile: enquiryState[i].mobile,
      comment: enquiryState[i].comment,
      date: enquiryState[i].updatedAt,
      status : (
        <>
          <select name="" className="form-control form-select" id="">
            <option value="">Set status</option>
            <option value="">pending</option>
            <option value="">refused</option>
            <option value="">accepted</option>
          </select>
        </>
      ),
      action: (
        <>
          <Link to="/" className="fs-3 text-warning pe-3">
            <AiFillEdit />
          </Link>
          <Link to="/" className="fs-3 text-danger">
            <AiFillEdit />
          </Link>
        </>
      ),
    });
  }
  return (
    <div>
      <h2 className="py-2">liste des commentaires</h2>
      <Table columns={columns} dataSource={data} />;
    </div>
  );
};

export default Commentaire;
