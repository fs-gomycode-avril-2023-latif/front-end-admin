import React, { useEffect } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { AiFillEdit } from "react-icons/ai";
import { AiFillDelete } from "react-icons/ai";
import { getCoupons } from "../../features/coupons/couponSlice";

const columns = [
  {
    title: "Sno",
    dataIndex: "key",
  },
  {
    title: "Name",
    dataIndex: "name",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.name.length - b.name.length,
  },
  {
    title: "Reduction",
    dataIndex: "discount",
  },
  {
    title: "Date d'expiration",
    dataIndex: "expire",
  },
  {
    title: "Action",
    dataIndex: "action",
  },
];

const Coupon = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCoupons());
  }, []);

  const couponState = useSelector((state) => state.coupon.coupons);
  const data1 = [];

  for (let i = 0; i < couponState.length; i++) {
    data1.push({
      key: i + 1,
      name: couponState[i].nom,
      discount: `${couponState[i].discount} %` ,
      expire: new Date(couponState[i].expire).toLocaleString() ,
      action: (
        <>
          <Link to={`/admin/coupon/${couponState[i]._id}`} className="text-decoration-none text-warning pe-2">
            <AiFillEdit className="fs-5 " /> Modifier
          </Link>
          <Link className="text-decoration-none text-danger ">
            <AiFillDelete className="fs-5 " /> Supprimer
          </Link>
        </>
      ),
    });
  }

  return (
    <>
      <h3>La liste des marques</h3>
      <Table columns={columns} dataSource={data1} />;
    </>
  );
};

export default Coupon;
