import React, { useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import InputComponent from "../../components/InputComponent";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { createCoupon, resetState } from "../../features/coupons/couponSlice";

let userSchema = Yup.object().shape({
  nom: Yup.string().required("ce champ doit etre rempli"),
  expire: Yup.string().required("ce champ doit etre rempli"),
  discount: Yup.string().required("ce champ doit etre rempli"),
});

const AddCoupon = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const newCoupon = useSelector((state) => state.coupon);
  const { isLoading, isError, isSuccess, createdCoupon } = newCoupon;

  useEffect(() => {
    if (isSuccess && createdCoupon) {
      toast.success("Nouveau coupon ajoute avec sucess");
    }
    if (isError) {
      toast.error("Echec lors de l'ajout de la nouveau coupon");
    }
  }, [isLoading, isError, isSuccess, createdCoupon]);

  const formik = useFormik({
    initialValues: {
      nom: "",
      expire: "",
      discount: "",
    },
    validationSchema: userSchema,

    onSubmit: async (values) => {
      dispatch(createCoupon(values));
      formik.resetForm();
      setTimeout(() => {
        dispatch(resetState())
        navigate("/admin/list-coupon");
      }, 3000);
      try {
      } catch (error) {
        // Gérer les erreurs
        throw new Error(error);
      }
    },
  });
  return (
    <form className="row g-3" onSubmit={formik.handleSubmit}>
      <h4 className="text-center mb-4 fw-bold">
        Formulaire d'ajout d'un coupon
      </h4>
      <div className="col-md-12">
        <InputComponent
          type="text"
          name="nom"
          placeholder="Nom du coupon"
          onch={formik.handleChange("nom")}
          onblr={formik.handleBlur("nom")}
          val={formik.values.nom}
        />
        <div className="text-danger">
          {formik.touched.nom && formik.errors.nom ? (
            <div>{formik.errors.nom}</div>
          ) : null}
        </div>
      </div>
      <div className="col-md-12">
        <InputComponent
          type="date"
          name="expire"
          placeholder="Date d'expiration"
          onch={formik.handleChange("expire")}
          onblr={formik.handleBlur("expire")}
          val={formik.values.expire}
        />
        <div className="text-danger">
          {formik.touched.expire && formik.errors.expire ? (
            <div>{formik.errors.expire}</div>
          ) : null}
        </div>
      </div>
      <div className="col-md-12">
        <InputComponent
          type="number"
          name="discount"
          placeholder="Reduction"
          min={1}
          max={100}
          onch={formik.handleChange("discount")}
          onblr={formik.handleBlur("discount")}
          val={formik.values.discount}
        />
        <div className="text-danger">
          {formik.touched.discount && formik.errors.discount ? (
            <div>{formik.errors.discount}</div>
          ) : null}
        </div>
      </div>

      <button type="submit" className="btn btn-lg btn-primary fs-5">
        Envoyer
      </button>
    </form>
  );
};

export default AddCoupon;
