import React, { useEffect } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { getBrands } from "../../features/brands/brandSlice";
import { Link } from "react-router-dom";
import { AiFillEdit } from "react-icons/ai";
import { AiFillDelete } from "react-icons/ai";
import { getRooms } from "../../features/rooms/roomSlice";

const columns = [
  {
    title: "Sno",
    dataIndex: "key",
  },
  {
    title: "Name",
    dataIndex: "name",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.name.length - b.name.length,
  },
  {
    title: "Categories",
    dataIndex: "category",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.category.length - b.category.length,
  },
  {
    title: "Prix",
    dataIndex: "price",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.price.length - b.price.length,
  },
  {
    title: "Marques",
    dataIndex: "brand",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.brand.length - b.brand.length,
  },
  {
    title: "Capacite",
    dataIndex: "capacity",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.capacity.length - b.capacity.length,
  },
  {
    title: "Description",
    dataIndex: "description",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.description.length - b.description.length,
  },
  {
    title: "Action",
    dataIndex: "action",
  },
];

const Room = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getRooms());
  }, []);
  const roomState = useSelector((state) => state.room.rooms);
  const data1 = [];

  for (let i = 0; i < roomState.length; i++) {
    data1.push({
      key: i + 1,
      name: roomState[i].nochambre,
      category: roomState[i].category,
      price: roomState[i].price,
      brand: roomState[i].brand,
      capacity: roomState[i].capacity,
      description: roomState[i].description,
      action: (
        <>
          <Link
            to={`/admin/brand/${roomState[i]._id}`}
            className="text-decoration-none text-warning pe-2"
          >
            <AiFillEdit className="fs-5 " /> Modifier
          </Link>
          <Link className="text-decoration-none text-danger ">
            <AiFillDelete className="fs-5 " /> Supprimer
          </Link>
        </>
      ),
    });
  }
  return (
    <>
      <h3>La liste des chambres</h3>
      <Table columns={columns} dataSource={data1} />;
    </>
  );
};

export default Room;
