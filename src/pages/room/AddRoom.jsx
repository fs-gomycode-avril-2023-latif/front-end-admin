import Dropzone from "react-dropzone";

// formulaire
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import { useFormik } from "formik";
import * as Yup from "yup";
import InputComponent from "../../components/InputComponent";
import { getCategorys } from "../../features/categorys/categorySlice";
import { deleteImg, uploadImg } from "../../features/upload/uploadSlice";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getBrands } from "../../features/brands/brandSlice";
import { createRoom, resetState } from "../../features/rooms/roomSlice";
import { useNavigate } from "react-router-dom";

let userSchema = Yup.object().shape({
  nochambre: Yup.string().required("ce champ doit etre rempli"),
  category: Yup.string().required("ce champ doit etre rempli"),
  price: Yup.string().required("ce champ doit etre rempli"),
  brand: Yup.string().required("ce champ doit etre rempli"),
  capacity: Yup.string().required("ce champ doit etre rempli"),
  description: Yup.string().required("ce champ doit etre rempli"),
});

const AddRoom = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate()

  useEffect(() => {
    dispatch(getBrands());
    dispatch(getCategorys());
  }, []);

  const categoryState = useSelector((state) => state.category.categorys);
  const brandState = useSelector((state) => state.brand.brands);
  const imgState = useSelector((state) => state.upload.images);
  const newRoom = useSelector((state) => state.room);
  const { isLoading, isError, isSuccess, createdRoom } = newRoom;

  useEffect(() => {
    if (isSuccess && createdRoom) {
      toast.success("Nouvelle chambre ajoute avec sucess");
    }
    if (isError) {
      toast.error("Echec lors de l'ajout de la nouvelle chambre");
    }
  }, [isLoading, isError, isSuccess, createdRoom]);

  const formik = useFormik({
    initialValues: {
      nochambre: "",
      category: "",
      price: "",
      description: "",
      brand: "",
      capacity: "",
      images: " ",
      tags: "",
    },
    validationSchema: userSchema,
    // onSubmit: (values) => {
    //   // dispatch(login(values));
    //   alert(JSON.stringify(values));
    //   dispatch(createRoom(values));
    // },
    onSubmit: async (values) => {
        dispatch(createRoom(values));
      formik.resetForm();
      setTimeout(() => {
        dispatch(resetState())
        navigate("/admin/list-room");
      }, 3000);
      try {
      } catch (error) {
        // Gérer les erreurs
        throw new Error(error);
      }
    },
  });

  const img = [];
  imgState.forEach((elt) => {
    img.push({
      public_id: elt.asset_id,
      url: elt.url,
    });
  });

  useEffect(() => {
    // Ici, nous mettons à jour les valeurs du formulaire avec les public_ids et asset_ids des images téléchargées
    const updatedImages = imgState.map((item) => ({
      ...item,
      public_id: item.public_id,
      asset_id: item.asset_id,
    }));

    formik.setFieldValue("images", updatedImages);
  }, [imgState]);

  return (
    <form className="row g-3" onSubmit={formik.handleSubmit}>
      <h4 className="text-center mb-4 fw-bold">
        Formulaire d'ajout d'une chambre
      </h4>
      <div className="col-md-12">
        <InputComponent
          type="text"
          name="nochambre"
          placeholder="Numero de chambre"
          onch={formik.handleChange("nochambre")}
          onblr={formik.handleBlur("nochambre")}
          val={formik.values.nochambre}
        />
        <div className="text-danger">
          {formik.touched.nochambre && formik.errors.nochambre ? (
            <div>{formik.errors.nochambre}</div>
          ) : null}
        </div>
      </div>
      <div className="row mb-3 ">
        <div className="col-md-6">
          <InputComponent
            type="text"
            name="price"
            placeholder="Prix"
            onch={formik.handleChange("price")}
            val={formik.values.price}
          />
          <div className="text-danger">
            {formik.touched.price && formik.errors.price ? (
              <div>{formik.errors.price}</div>
            ) : null}
          </div>
        </div>
        <div className="col-md-6">
          <InputComponent
            type="number"
            name="capacity"
            placeholder="Capacite d'accueil"
            onch={formik.handleChange("capacity")}
            val={formik.values.capacity}
          />
          <div className="text-danger">
            {formik.touched.capacity && formik.errors.capacity ? (
              <div>{formik.errors.capacity}</div>
            ) : null}
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-md-6">
          <select
            name="brand"
            onChange={formik.handleChange("brand")}
            onBlur={formik.handleBlur("brand")}
            value={formik.values.brand}
            className="form-control py-3 mb-3"
            id=""
          >
            <option value="default">select brand</option>
            {brandState.map((item, index) => {
              return (
                <option key={index} value={item.nom}>
                  {item.nom}
                </option>
              );
            })}
          </select>
          <div className="text-danger">
            {formik.touched.brand && formik.errors.brand ? (
              <div>{formik.errors.brand}</div>
            ) : null}
          </div>
        </div>

        <div className="col-md-6">
          <select
            name="category"
            onChange={formik.handleChange("category")}
            onBlur={formik.handleBlur("category")}
            value={formik.values.category}
            className="form-control py-3 mb-3"
            id=""
          >
            <option value="default">select category</option>
            {categoryState.map((item, index) => {
              return (
                <option key={index} value={item.nom}>
                  {item.nom}
                </option>
              );
            })}
          </select>
          <div className="text-danger">
            {formik.touched.category && formik.errors.category ? (
              <div>{formik.errors.category}</div>
            ) : null}
          </div>
        </div>
        <div className="col-md-6">
          <select
            name="tags"
            onChange={formik.handleChange("tags")}
            onBlur={formik.handleBlur("tags")}
            value={formik.values.tags}
            className="form-control py-3 mb-3"
            id=""
          >
            <option value="" disabled>
              select tags
            </option>
            <option value="featured">featured</option>
            <option value="popular">popular</option>
            <option value="special">special</option>
          </select>
          <div className="text-danger">
            {formik.touched.tags && formik.errors.tags ? (
              <div>{formik.errors.tags}</div>
            ) : null}
          </div>
        </div>
      </div>

      <div className="form-floating">
        {/* <label for="floatingTextarea">Description de la chambre</label> */}
        <ReactQuill
          theme="snow"
          name="description"
          onChange={(value) => formik.setFieldValue("description", value)}
          onBlur={() => formik.handleBlur("description")}
          value={formik.values.description}
        />

        <div className="text-danger">
          {formik.touched.description && formik.errors.description ? (
            <div>{formik.errors.description}</div>
          ) : null}
        </div>
      </div>
      <div className="bg-white border-1 p-5 text-center">
        <Dropzone
          onDrop={(acceptedFiles) => dispatch(uploadImg(acceptedFiles))}
        >
          {({ getRootProps, getInputProps }) => (
            <section>
              <div {...getRootProps()}>
                <input {...getInputProps()} />
                <p>Click to select files</p>
              </div>
            </section>
          )}
        </Dropzone>
      </div>
      <div className="showimages d-flex flex-wrap gap-3">
        {imgState.map((item, index) => {
          return (
            <div key={index} className="position-relative">
              <button
                onClick={() => dispatch(deleteImg(item.asset_id))}
                className="btn-close position-absolute"
                style={{ top: "10px", right: "10px" }}
              ></button>
              <img src={item.url} alt="" width={200} height={200} />
            </div>
          );
        })}
      </div>
      <div className="input-group mb-3">
        <button type="submit" className="btn btn-lg btn-primary fs-5">
          Envoyer
        </button>
      </div>
    </form>
  );
};

export default AddRoom;
