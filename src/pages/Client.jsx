import React, { useState } from "react";
import { ListTab } from "../components/ListTab";
import { Link } from "react-router-dom";
import { Modal } from "antd";

const Client = () => {
  // hooks btn
  const [open, setOpen] = useState(false);
  return (
    <div className="mt-4">
      <div className="d-flex align-items-center justify-content-between">
        <h4>liste des clients</h4>
        {/* BtnForm et modal  */}
        <Link
          className="text-deoration-none text-white btn btn-success "
          onClick={() => setOpen(true)}
        >
          Ajouter
        </Link>
        <Modal
          centered
          open={open}
          onOk={() => setOpen(false)}
          onCancel={() => setOpen(false)}
          width={1000}
        >
          {/* formulaire d'ajout  */}
          <form className="row g-3">
            <h4 className="text-center mb-4 fw-bold">
              Formulaire d'ajout d'un client
            </h4>
            <div className="col-md-6">
              <input
                type="text"
                className="form-control"
                name="nom"
                placeholder="Nom"
              />
            </div>
            <div className="col-md-6">
              <input
                type="text"
                className="form-control"
                name="prenom"
                placeholder="Prenom"
              />
            </div>
            <div className="col-md-6">
              <input
                type="text"
                className="form-control"
                name="email"
                placeholder="Email"
              />
            </div>
            <div className="col-md-6">
              <input
                type="text"
                className="form-control"
                name="noTel"
                placeholder="Numero de telephone"
              />
            </div>

            <div class="mb-3">
              <label for="formFile" class="form-label">
                photo de profile
              </label>
              <input
                class="form-control"
                type="file"
                name="photoProfile"
                id="formFile"
              />
            </div>

            <div className="col-12">
              <Link type="submit" className="btn btn-primary">
                Ajouter
              </Link>
            </div>
          </form>
        </Modal>
      </div>
      <div className="mt-3">
        <ListTab />
      </div>
    </div>
  );
};

export default Client;
