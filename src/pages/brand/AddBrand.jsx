import React, { useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import InputComponent from "../../components/InputComponent";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { createBrand, resetState } from "../../features/brands/brandSlice";
import { toast } from "react-toastify";

let userSchema = Yup.object().shape({
  nom: Yup.string().required("ce champ doit etre rempli"),
});

const AddBrand = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const newBrand = useSelector((state) => state.brand);
  const { isLoading, isError, isSuccess, createdBrand } = newBrand;

  useEffect(() => {
    if (isSuccess && createdBrand) {
      toast.success("Nouvelle marque ajoute avec sucess");
    }
    if (isError) {
      toast.error("Echec lors de l'ajout de la nouvelle marque");
    }
  }, [isLoading, isError, isSuccess, createdBrand]);

  const formik = useFormik({
    initialValues: {
      nom: "",
    },
    validationSchema: userSchema,

    onSubmit: async (values) => {
      dispatch(createBrand(values));
      formik.resetForm();
      setTimeout(() => {
        dispatch(resetState())
        navigate("/admin/listBrand");
      }, 3000);
      try {
      } catch (error) {
        // Gérer les erreurs
        throw new Error(error);
      }
    },
  });
  return (
    <form className="row g-3" onSubmit={formik.handleSubmit}>
      <h4 className="text-center mb-4 fw-bold">
        Formulaire d'ajout d'une brand
      </h4>
      <div className="col-md-12">
        <InputComponent
          type="text"
          name="nom"
          placeholder="Nom de la marque"
          onch={formik.handleChange("nom")}
          onblr={formik.handleBlur("nom")}
          val={formik.values.nom}
        />
        <div className="text-danger">
          {formik.touched.nom && formik.errors.nom ? (
            <div>{formik.errors.nom}</div>
          ) : null}
        </div>
      </div>

      <button type="submit" className="btn btn-lg btn-primary fs-5">
        Envoyer
      </button>
    </form>
  );
};

export default AddBrand;
