import React, { useEffect } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { getBrands } from "../../features/brands/brandSlice";
import { Link } from "react-router-dom";
import { AiFillEdit } from "react-icons/ai";
import { AiFillDelete } from "react-icons/ai";

const columns = [
  {
    title: "Sno",
    dataIndex: "key",
  },
  {
    title: "Name",
    dataIndex: "name",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.name.length - b.name.length,
  },
  {
    title: "Action",
    dataIndex: "action",
  },
];

const Brand = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getBrands());
  }, []);

  const brandState = useSelector((state) => state.brand.brands);
  const data1 = [];

  for (let i = 0; i < brandState.length; i++) {
    data1.push({
      key: i + 1,
      name: brandState[i].nom,
      action: (
        <>
          <Link to={`/admin/brand/${brandState[i]._id}`} className="text-decoration-none text-warning pe-2">
            <AiFillEdit className="fs-5 " /> Modifier
          </Link>
          <Link className="text-decoration-none text-danger ">
            <AiFillDelete className="fs-5 " /> Supprimer
          </Link>
        </>
      ),
    });
  }

  return (
    <>
      <h3>La liste des marques</h3>
      <Table columns={columns} dataSource={data1} />;
    </>
  );
};

export default Brand;
