import React from 'react'
import { Link } from "react-router-dom";
import { BsShop } from "react-icons/bs";
import InputComponent from '../components/InputComponent';

const ResetPassword = () => {
  return (
    <div
    className="container-fluid d-flex justify-content-center align-items-center"
    id="wrapper"
  >
    <div className="row rounded-5" id="forgot-wrapper">
      <div className="col">
        <div class="shadow-sm p-3 mb-5 bg-body-tertiary rounded">
          <div className="header-text text-secondary lead mb-4">
            {/* logo */}
            <div className=" text-center mb-5">
              <div>
                <Link to="/" className="text-decoration-none">
                  <BsShop className=" text-primary fs-3" />
                </Link>
                <span className="fs-5">Nom Entreprise</span>
              </div>
            </div>
            {/* logo */}
            <h2 className="fw-bold text-dark">Reinitialisation de mot de passe</h2>
            <p>Enter your password</p>
            <InputComponent type="password" placeholder="enter new password" />
            <InputComponent type="password" placeholder="confirm password" />
            <div className="input-group mb-3">
              <Link to="/" className="text-decoration-none text-white">
              <button className="btn btn-lg btn-primary fs-5">
                Send
              </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  )
}

export default ResetPassword