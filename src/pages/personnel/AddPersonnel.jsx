import Dropzone from "react-dropzone";

// formulaire
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import { useFormik } from "formik";
import * as Yup from "yup";
import InputComponent from "../../components/InputComponent";
import { deleteImg, uploadImg } from "../../features/upload/uploadSlice";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { createUser } from "../../features/personnels/personnelSlice";

let userSchema = Yup.object().shape({
  nom: Yup.string().required("ce champ doit etre rempli"),
  prenom: Yup.string().required("ce champ doit etre rempli"),
  poste: Yup.string().required("ce champ doit etre rempli"),
  roles: Yup.string().required("ce champ doit etre rempli"),
  email: Yup.string().required("ce champ doit etre rempli"),
  tel: Yup.string().required("ce champ doit etre rempli"),
  password: Yup.string().required("ce champ doit etre rempli"),
});

const AddPersonnel = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const imgState = useSelector((state) => state.upload.images);
  const newUser = useSelector((state) => state.personnel);
  const { isLoading, isError, isSuccess, createdUser } = newUser;

  useEffect(() => {
    if (isSuccess && createdUser) {
      toast.success("Nouvelle chambre ajoute avec sucess");
    }
    if (isError) {
      toast.error("Echec lors de l'ajout de la nouvelle chambre");
    }
  }, [isLoading, isError, isSuccess, createdUser]);

  const formik = useFormik({
    initialValues: {
      nom: "",
      prenom: "",
      roles: "",
      poste: "",
      email: "",
      tel: "",
      password: "",
    },
    validationSchema: userSchema,
    // onSubmit: (values) => {
    //   // dispatch(login(values));
    //   alert(JSON.stringify(values));
    //   dispatch(createRoom(values));
    // },
    onSubmit: async (values) => {
      dispatch(createUser(values));
      formik.resetForm();
      setTimeout(() => {
        navigate("/admin/list-personnel");
      }, 3000);
      try {
      } catch (error) {
        // Gérer les erreurs
        throw new Error(error);
      }
    },
  });

  const img = [];
  imgState.forEach((elt) => {
    img.push({
      public_id: elt.asset_id,
      url: elt.url,
    });
  });

  useEffect(() => {
    // Ici, nous mettons à jour les valeurs du formulaire avec les public_ids et asset_ids des images téléchargées
    const updatedImages = imgState.map((item) => ({
      ...item,
      public_id: item.public_id,
      asset_id: item.asset_id,
    }));

    formik.setFieldValue("images", updatedImages);
  }, [imgState]);

  return (
    <form className="row g-3" onSubmit={formik.handleSubmit}>
      <h4 className="text-center mb-4 fw-bold">
        Formulaire d'ajout du personnel
      </h4>
      <div className="col-md-12">
        <InputComponent
          type="text"
          name="nom"
          placeholder="Nom"
          onch={formik.handleChange("nom")}
          onblr={formik.handleBlur("nom")}
          val={formik.values.nom}
        />
        <div className="text-danger">
          {formik.touched.nom && formik.errors.nom ? (
            <div>{formik.errors.nom}</div>
          ) : null}
        </div>
      </div>
      <div className="col-md-12">
        <InputComponent
          type="text"
          name="prenom"
          placeholder="Prenom"
          onch={formik.handleChange("prenom")}
          onblr={formik.handleBlur("prenom")}
          val={formik.values.prenom}
        />
        <div className="text-danger">
          {formik.touched.prenom && formik.errors.prenom ? (
            <div>{formik.errors.prenom}</div>
          ) : null}
        </div>
      </div>
      <div className="row mb-3 ">
        <div className="col-md-6">
          <InputComponent
            type="text"
            name="email"
            placeholder="Email"
            onch={formik.handleChange("email")}
            val={formik.values.email}
          />
          <div className="text-danger">
            {formik.touched.email && formik.errors.email ? (
              <div>{formik.errors.email}</div>
            ) : null}
          </div>
        </div>
        <div className="col-md-6">
          <InputComponent
            type="text"
            name="poste"
            placeholder="Poste"
            onch={formik.handleChange("poste")}
            val={formik.values.poste}
          />
          <div className="text-danger">
            {formik.touched.poste && formik.errors.poste ? (
              <div>{formik.errors.poste}</div>
            ) : null}
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-md-6">
          <select
            name="roles"
            onChange={formik.handleChange("roles")}
            onBlur={formik.handleBlur("roles")}
            value={formik.values.roles}
            className="form-control py-3 mb-3"
            id=""
          >
            <option value="default">select role</option>

            <option key="admin" value="admin">
              admin
            </option>
            <option key="employe" value="employe">
              employe
            </option>
          </select>
          <div className="text-danger">
            {formik.touched.roles && formik.errors.roles ? (
              <div>{formik.errors.roles}</div>
            ) : null}
          </div>
        </div>
        <div className="col-md-6">
          <InputComponent
            type="text"
            name="password"
            placeholder="Mot de passe"
            onch={formik.handleChange("password")}
            val={formik.values.password}
          />
          <div className="text-danger">
            {formik.touched.password && formik.errors.password ? (
              <div>{formik.errors.password}</div>
            ) : null}
          </div>
        </div>
      </div>
      <div className="col-md-12">
        <InputComponent
          type="text"
          name="tel"
          placeholder="Telephone"
          onch={formik.handleChange("tel")}
          onblr={formik.handleBlur("tel")}
          val={formik.values.tel}
        />
        <div className="text-danger">
          {formik.touched.tel && formik.errors.tel ? (
            <div>{formik.errors.tel}</div>
          ) : null}
        </div>
      </div>
      <div className="bg-white border-1 p-5 text-center">
        <Dropzone
          onDrop={(acceptedFiles) => dispatch(uploadImg(acceptedFiles))}
        >
          {({ getRootProps, getInputProps }) => (
            <section>
              <div {...getRootProps()}>
                <input {...getInputProps()} />
                <p>Click to select files</p>
              </div>
            </section>
          )}
        </Dropzone>
      </div>
      <div className="showimages d-flex flex-wrap gap-3">
        {imgState.map((item, index) => {
          return (
            <div key={index} className="position-relative">
              <button
                onClick={() => dispatch(deleteImg(item.asset_id))}
                className="btn-close position-absolute"
                style={{ top: "10px", right: "10px" }}
              ></button>
              <img src={item.url} alt="" width={200} height={200} />
            </div>
          );
        })}
      </div>
      <button type="submit" className="btn btn-lg btn-primary fs-5">
        Envoyer
      </button>
    </form>
  );
};

export default AddPersonnel;
