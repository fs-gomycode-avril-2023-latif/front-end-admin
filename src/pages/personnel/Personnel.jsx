import React, { useEffect } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { getUsers } from "../../features/personnels/personnelSlice";
import { Link } from "react-router-dom";
import { AiFillEdit } from "react-icons/ai";
import { AiFillDelete } from "react-icons/ai";

const columns = [
  {
    title: "Sno",
    dataIndex: "key",
  },
  {
    title: "Nom",
    dataIndex: "nom",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.nom.length - b.nom.length,
  },
  {
    title: "prenom",
    dataIndex: "prenom",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.prenom.length - b.prenom.length,
  },
  {
    title: "Poste",
    dataIndex: "poste",
  },
  {
    title: "Telepheone",
    dataIndex: "tel",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.tel.length - b.tel.length,
  },
  {
    title: "Email",
    dataIndex: "email",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.email.length - b.email.length,
  },
  {
    title: "Role",
    dataIndex: "role",
    defaultSortOrder: "descend",
    sorter: (a, b) => a.role.length - b.role.length,
  },
  {
    title: "Action",
    dataIndex: "action",
  },
];

const Personnel = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUsers());
  }, []);
  const userState = useSelector((state) => state.personnel.personnels);
  const data1 = [];

  for (let i = 0; i < userState.length; i++) {
    data1.push({
      key: i + 1,
      nom: userState[i].nom,
      prenom: userState[i].prenom,
      poste: userState[i].poste,
      email: userState[i].email,
      tel: userState[i].tel,
      role: userState[i].roles[0],
      action: (
        <>
          <Link
            to={`/admin/brand/${userState[i]._id}`}
            className="text-decoration-none text-warning pe-2"
          >
            <AiFillEdit className="fs-5 " /> Modifier
          </Link>
          <Link className="text-decoration-none text-danger ">
            <AiFillDelete className="fs-5 " /> Supprimer
          </Link>
        </>
      ),
    });
  }
  return (
    <>
      <h3>La liste du personnel</h3>
      <Table columns={columns} dataSource={data1} />;
    </>
  );
};

export default Personnel;
