import React, { useState } from "react";
import { ListTab } from "../components/ListTab";
import { Link } from "react-router-dom";
import { Modal } from "antd";

const TypeChambre = () => {
  // hooks btn
  const [open, setOpen] = useState(false);
  return (
    <div className="mt-4">
      <div className="d-flex align-items-center justify-content-between">
        <h4>liste des types de chambres</h4>
        {/* BtnForm et modal  */}
        <Link
          className="text-deoration-none text-white btn btn-success "
          onClick={() => setOpen(true)}
        >
          Ajouter
        </Link>
        <Modal
          centered
          open={open}
          onOk={() => setOpen(false)}
          onCancel={() => setOpen(false)}
          width={1000}
        >
          {/* formulaire d'ajout  */}
          <form className="row g-3">
            <h4 className="text-center mb-4 fw-bold">
              Formulaire d'ajout d'une chambre
            </h4>
            <div className="col-md-12">
              <input
                type="text"
                className="form-control"
                name="nomTypeChambre"
                placeholder="Nom du type de chambre"
              />
            </div>
            <div class="form-floating">
              <textarea
                class="form-control"
                placeholder="Description de la chambre"
                id="floatingTextarea"
              ></textarea>
              <label for="floatingTextarea">Description</label>
            </div>

            <div className="col-12">
              <Link type="submit" className="btn btn-primary">
                Ajouter
              </Link>
            </div>
          </form>
        </Modal>
      </div>
      <div className="mt-3">
        <ListTab />
      </div>
    </div>
  )
}

export default TypeChambre