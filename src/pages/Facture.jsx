import React from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { Link } from "react-router-dom";
import { AiFillEdit } from "react-icons/ai";
import { getOrders } from "../features/auth/authSlice";

const columns = [
  {
    title: "Key",
    dataIndex: "key",
  },
  {
    title: "Date",
    dataIndex: "date",
  },
  {
    title: "Nom du client",
    dataIndex: "nom",
  },
  {
    title: "Details de la Chambre",
    dataIndex: "room",
  },
  {
    title: "Prix Total de la (des) Chambre (s)",
    dataIndex: "prix",
  },
  {
    title: "Actions",
    dataIndex: "action",
  },
];
const Facture = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getOrders());
  }, []);
  const orderState = useSelector((state) => state.auth.orders);
  const data = [];
  // console.log(orderState);
  for (let i = 0; i < orderState.length; i++) {
    data.push({
      key: i + 1,
      nom: `${orderState[i].orderBy.nom} ${orderState[i].orderBy.prenom}`,
      room: orderState[i].rooms.map((item, index) => {
        return (
          <span key={index} className="text-success d-block">
            <ul>
              <li>
                Numero de chambre : {item.room.nochambre} a {item.room.price}{" "}
                Francs/CFA
              </li>
              <li>Prix de chambre : {item.room.price} Francs/CFA</li>
              <li>
                Avec une capacite d'accueil de {item.room.capacity} personnes
              </li>
            </ul>
          </span>
        );
      }),
      prix: `${orderState[i].paymentInternet.amount} ${orderState[i].paymentInternet.currency} `,
      date: new Date(orderState[i].createdAt).toLocaleString(),
      action: (
        <>
          <Link to="/" className="fs-3 text-warning pe-3">
            <AiFillEdit />
          </Link>
          <Link to="/" className="fs-3 text-danger">
            <AiFillEdit />
          </Link>
        </>
      ),
    });
  }
  return (
    <div>
      <h2 className="py-2">liste des Factures</h2>
      <Table columns={columns} dataSource={data} />
    </div>
  );
};

export default Facture;
