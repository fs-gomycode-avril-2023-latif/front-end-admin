import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../features/auth/authSlice";
import personnelReducer from "../features/personnels/personnelSlice";
import roomReducer from "../features/rooms/roomSlice";
import brandReducer from "../features/brands/brandSlice";
import categoryReducer from "../features/categorys/categorySlice";
import serviceReducer from "../features/services/serviceSlice";
import couponReducer from "../features/coupons/couponSlice";
import enquiryReducer from "../features/enquirys/enquirySlice";
import uploadReducer from "../features/upload/uploadSlice";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    personnel: personnelReducer,
    room: roomReducer,
    brand: brandReducer,
    category: categoryReducer,
    service: serviceReducer,
    coupon: couponReducer,
    enquiry: enquiryReducer,
    upload: uploadReducer,
  },
  devTools: true,
});
