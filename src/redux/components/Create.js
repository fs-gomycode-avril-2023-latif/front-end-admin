import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addUser } from "../reducers/userSlice";
import { useNavigate } from "react-router-dom";

const Create = () => {
  const [dataForm, setDataForm] = useState({
    name: "",
    email: "",
  });

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleChange = (e) => {
    setDataForm({ ...dataForm, [e.target.name]: e.target.value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    alert("nouvel utilisateur ajouté");
    dispatch(addUser(dataForm));
    navigate("/");
  };
  return (
    <div className="d-flex w-100 vh-100 justify-content-center align-items-center">
      <div className="w-50 border bg-secondary text-white p-5">
        <form onSubmit={handleSubmit}>
          <h3>Add New User</h3>
          <div>
            <input
              type="hidden"
              name="id"
              onChange={handleChange}
              defaultValue={Date.now()}
            />
            <label htmlFor="name">Name</label>
            <input
              type="text"
              name="name"
              onChange={handleChange}
              className="form-control"
            />
          </div>
          <div>
            <label htmlFor="email">Email</label>
            <input
              type="text"
              name="email"
              onChange={handleChange}
              className="form-control"
            />
          </div>
          <br />
          <button className="btn btn-info">Submit </button>
        </form>
      </div>
    </div>
  );
};

export default Create;
