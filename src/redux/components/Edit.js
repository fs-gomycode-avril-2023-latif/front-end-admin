import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { updateUser } from "../reducers/userSlice";

const Edit = () => {
  const { id } = useParams();

  const users = useSelector((state) => state.users);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const existingUser = users.filter((user) => user.id == id);
  const { email, name } = existingUser[0];

  const [uemail, setEmail] = useState(email);
  const [uname, setName] = useState(name);

  const handleUpdate = (e) => {
    e.preventDefault();
    const data = {
      id: id,
      name: uname,
      email: uemail,
    };
    dispatch(updateUser(data));
    navigate("/");
  };

  return (
    <div className="d-flex w-100 vh-100 justify-content-center align-items-center">
      <div className="w-50 border bg-secondary text-white p-5">
        <form onSubmit={handleUpdate}>
          <h3>Update User</h3>
          <div>
            <label htmlFor="name">Name</label>
            <input
              type="text"
              defaultValue={uname}
              onChange={(e) => setEmail(e.target.value)}
              name="name"
              className="form-control"
            />
          </div>
          <div>
            <label htmlFor="email">Email</label>
            <input
              type="text"
              defaultValue={uemail}
              onChange={(e) => setName(e.target.value)}
              name="email"
              className="form-control"
            />
          </div>
          <br />
          <button className="btn btn-info">Update </button>
        </form>
      </div>
    </div>
  );
};
export default Edit;
