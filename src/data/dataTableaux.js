const colonne = [
  {
    title: "SNo",
    dataIndex: "key",
  },
  {
    title: "Name",
    dataIndex: "name",
  },
  {
    title: "Product",
    dataIndex: "product",
  },
  {
    title: "Status",
    dataIndex: "status",
  },
];
const dataTab = [];
for (let i = 0; i < 46; i++) {
  dataTab.push({
    key: i,
    name: `Edward King ${i}`,
    product: 32,
    status: `test ${i}`,
  });
}

export {colonne , dataTab}
