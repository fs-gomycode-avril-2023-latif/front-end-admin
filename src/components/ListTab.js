import React, { useEffect } from "react";
import { Space, Table } from "antd";
import { Link } from "react-router-dom";
import { AiFillEdit } from "react-icons/ai";
import { useDispatch, useSelector } from "react-redux";
import { getUsers } from "../features/personnels/personnelSlice";

const columns = [
  {
    title: "No Chambre",
    dataIndex: "noChambre", //les données sont recus ici
    key: "NoChambre",
    render: (text) => (
      <Link className="text-decoration-none text-dark">{text}</Link>
    ),
  },
  {
    title: "Type de Chambre",
    dataIndex: "typeChambre",
    key: "TypeChambre",
  },
  {
    title: "Prix",
    dataIndex: "prix",
    key: "prix",
  },
  {
    title: "Capacite",
    key: "capacite",
    dataIndex: "capacite",
    // render: (_, { tags }) => (
    //   <>
    //     {tags.map((tag) => {
    //       let color = tag.length > 5 ? "geekblue" : "green";
    //       if (tag === "loser") {
    //         color = "volcano";
    //       }
    //       return (
    //         <Tag color={color} key={tag}>
    //           {tag.toUpperCase()}
    //         </Tag>
    //       );
    //     })}
    //   </>
    // ),
  },

  {
    title: "Disponibilité",
    key: "disponibilite",
    dataIndex: "disponibilite",
    render: (_, record) => {
      let color;
      if (record.disponibilite === "occupe") {
        color = "text-danger";
      } else if (record.disponibilite === "renovation") {
        color = "text-warning";
      } else {
        color = "text-success";
      }
      return (
        <>
          <p className={color}>{record.disponibilite}</p>
        </>
      );
    },
  },
  {
    title: "Description",
    dataIndex: "description",
    key: "description",
  },
  {
    title: "Action",
    key: "action",
    render: (_, record) => (
      <Space size="middle">
        <Link to="edit" className="text-decoration-none text-warning ">
          <AiFillEdit className="fs-5 " /> Modifier
        </Link>
        <Link className="text-decoration-none text-danger ">
          <AiFillEdit className="fs-5 " /> Supprimer
        </Link>
      </Space>
    ),
  },
];
// ID
// - Numéro de chambre
// - Type de chambre (simple, double, suite, etc.)
// - Prix par nuit
// - Capacité (nombre de personnes)
// - Description de la chambre
// - Disponibilité
const data = [
  {
    key: "1",
    noChambre: "John Brown",
    typeChambre: "Suite",
    prix: `52€`,
    capacite: 5,
    disponibilite: "occupe",
    description: "New York No. 1 Lake Park",
    tags: ["nice", "developer"],
  },
  {
    key: "2",
    noChambre: "Jim Green",
    typeChambre: "Studio",
    prix: `42€`,
    capacite: 5,
    disponibilite: "ouvert",
    description: "London No. 1 Lake Park",
  },
  {
    key: "3",
    noChambre: "Joe Black",
    typeChambre: "Chambre",
    prix: `32€`,
    capacite: 5,
    disponibilite: "renovation",
    description: "Sydney No. 1 Lake Park",
    tags: ["cool", "teacher"],
  },
];

export const ListTab = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getUsers());
  }, []);
  const personnelState = useSelector((state) => state.personnel.personnels);
  return <Table columns={columns} dataSource={data} />;
};
