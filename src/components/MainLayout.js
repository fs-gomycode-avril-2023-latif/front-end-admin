import React, { useState } from "react";
import {
  AiOutlineDashboard,
  AiOutlineShoppingCart,
  AiOutlineUser,
  AiOutlineBgColors,
  AiOutlinePicRight,
  AiOutlinePicLeft,
  AiTwotoneBell,
  AiFillHome,
} from "react-icons/ai";

import { BsFillPeopleFill } from "react-icons/bs";
import { SiBrandfolder } from "react-icons/si";
import {
  BiCategoryAlt,
  BiLogoBlogger,
  BiListCheck,
  BiSolidGift,
  BiCategory,
} from "react-icons/bi";
import { FaClipboardList, FaFileInvoiceDollar } from "react-icons/fa";
import { Layout, Menu, Button, theme } from "antd";
import { Link, Outlet, useNavigate } from "react-router-dom";
import av from "../assets/images/users/avatar1.png";
import { DownOutlined } from "@ant-design/icons";
import { Dropdown, Space } from "antd";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const items = [
  {
    label: <Link href="https://www.antgroup.com">1st menu item</Link>,
    key: "0",
  },
  {
    label: <Link href="https://www.aliyun.com">2nd menu item</Link>,
    key: "1",
  },
  {
    type: "divider",
  },
  {
    label: "3rd menu item",
    key: "3",
  },
];

const { Header, Sider, Content } = Layout;

const MainLayout = () => {
  const [collapsed, setCollapsed] = useState(false);
  const navigate = useNavigate();

  const {
    token: { colorBgContainer },
  } = theme.useToken();

  return (
    <Layout className="vh-100">
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo">
          <h2 className="text-white fs-5 text-center py-3 mb-0">
            <span className="lg-logo">Latif Market</span>
            <span className="sm-logo">LM</span>
          </h2>
        </div>
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={["/admin"]}
          onClick={({ key }) => {
            if (key === "signout") {
            } else {
              navigate(key);
              // console.log(navigate(key))
            }
          }}
          items={[
            {
              key: "/admin",
              icon: <AiOutlineDashboard className="fs-4" />,
              label: "Dashboard",
            },
            {
              key: "profile",
              icon: <AiOutlineUser className="fs-4" />,
              label: "Profile",
            },
            {
              key: "brand",
              icon: <SiBrandfolder className="fs-4" />,
              label: "Brand",
              children: [
                {
                  key: "listBrand", // Cette clé doit être unique
                  icon: <SiBrandfolder className="fs-4" />,
                  label: "Brand list",
                },
                {
                  key: "add-brand", // Cette clé doit être unique
                  icon: <SiBrandfolder className="fs-4" />,
                  label: "Add brand",
                },
              ],
            },
            {
              key: "category",
              icon: <BiCategoryAlt className="fs-4" />,
              label: "Category",
              children : [
                {
                  key: "list-categorys",
                  icon: <BiCategoryAlt className="fs-4" />,
                  label: "liste des categories",
                },
                {
                  key: "add-category",
                  icon: <BiCategoryAlt className="fs-4" />,
                  label: "ajouter une categorie",
                },
              ]
            },
            {
              key: "coupon",
              icon: <BiCategoryAlt className="fs-4" />,
              label: "Coupon",
              children : [
                {
                  key: "list-coupon",
                  icon: <BiCategoryAlt className="fs-4" />,
                  label: "liste des coupons",
                },
                {
                  key: "add-coupon",
                  icon: <BiCategoryAlt className="fs-4" />,
                  label: "ajouter un coupon",
                },
              ]
            },
            {
              key: "chambre",
              icon: <AiFillHome className="fs-4" />,
              label: "Chambre",
              children : [
                {
                  key: "list-room",
                  icon: <BiCategoryAlt className="fs-4" />,
                  label: "liste des chambres",
                },
                {
                  key: "add-room",
                  icon: <BiCategoryAlt className="fs-4" />,
                  label: "ajouter une chambre",
                },
              ]
            },
            {
              key: "catalog",
              icon: <AiOutlineShoppingCart className="fs-4" />,
              label: "Admin",
              children: [
                {
                  key: "add-product",
                  icon: <AiOutlineShoppingCart className="fs-4" />,
                  label: "add Product",
                },

                {
                  key: "list-brand",
                  icon: <SiBrandfolder className="fs-4" />,
                  label: "Brand list ",
                },

                {
                  key: "list-category",
                  icon: <BiCategoryAlt className="fs-4" />,
                  label: "Category list",
                },
                {
                  key: "color",
                  icon: <AiOutlineBgColors className="fs-4" />,
                  label: "Color",
                },
                {
                  key: "list-color",
                  icon: <AiOutlineBgColors className="fs-4" />,
                  label: "Color list",
                },
              ],
            },

            {
              key: "clients",
              icon: <BsFillPeopleFill className="fs-4" />,
              label: "Clients",
            },
            {
              key: "reservation",
              icon: <BiListCheck className="fs-4" />,
              label: "Reservation",
            },
            {
              key: "service",
              icon: <BiSolidGift className="fs-4" />,
              label: "Service",
              children : [
                {
                  key: "list-service",
                  icon: <BiSolidGift className="fs-4" />,
                  label: "liste des services",
                },
                {
                  key: "add-service",
                  icon: <BiSolidGift className="fs-4" />,
                  label: "ajouter un service",
                },
              ]
            },
            {
              key: "facture",
              icon: <FaFileInvoiceDollar className="fs-4" />,
              label: "Facture",
            },
            {
              key: "personnel",
              icon: <FaClipboardList className="fs-4" />,
              label: "Personnel",
              children : [
                {
                  key: "list-personnel",
                  icon: <FaClipboardList className="fs-4" />,
                  label: "liste des personnels",
                },
                {
                  key: "add-personnel",
                  icon: <FaClipboardList className="fs-4" />,
                  label: "ajouter un personnel",
                },
              ]
              
            },
            {
              key: "commentaire",
              icon: <BiLogoBlogger className="fs-4" />,
              label: "Commentaire",
            },
            {
              key: "type-chambre",
              icon: <BiCategory className="fs-4" />,
              label: "Type de chambre",
            },
          ]}
        />
      </Sider>
      <Layout>
        <Header
          className="d-flex justify-content-between ps-1 pe-5"
          style={{
            padding: 0,
            background: colorBgContainer,
          }}
        >
          <Button
            type="text"
            icon={collapsed ? <AiOutlinePicRight /> : <AiOutlinePicLeft />}
            onClick={() => setCollapsed(!collapsed)}
            style={{
              fontSize: "16px",
              width: 64,
              height: 64,
            }}
          />
          <div className="d-flex gap-2 align-items-center">
            <div>
              <Dropdown
                menu={{
                  items,
                }}
                trigger={["click"]}
              >
                <Link onClick={(e) => e.preventDefault()}>
                  <Space>
                    <div className="position-relative text-dark">
                      <AiTwotoneBell className="fs-5" />
                      <DownOutlined />
                      <span className="badge bg-warning rounded-circle p-1 position-absolute">
                        4
                      </span>
                    </div>
                  </Space>
                </Link>
              </Dropdown>
            </div>

            <div className="d-flex gap-3 align-items-center">
              <div>
                <img src={av} alt=" user" width={30} height={30} />
              </div>
              <div className="d-flex flex-column justify-content-center">
                <h5 className="m-0 text-dark">latif gneba Idriss</h5>
                <p className="m-0 text-secondary lead">juniorgneba@gmail.com</p>
              </div>
            </div>
          </div>
        </Header>
        <Content
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
          }}
        >
          <ToastContainer
            position="top-right"
            autoClose={1000}
            hideProgressBar={false}
            newestOnTop={true}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            theme="light"
          />
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  );
};

export default MainLayout;
