import React from "react";

const InputComponent = (props) => {
  const { type, placeholder,nam,val,onch,onblr } = props;
  return (
    <div className="input-group mt-3">
      <input
        type={type}
        className="form-control  fs-5"
        placeholder={placeholder}
        name={nam}
        value={val}
        onChange={onch}
        onBlur={onblr}
      />
    </div>
  );
};

export default InputComponent;
