import React from "react";
import { Link } from "react-router-dom";

const AjoutForm = () => {
  return (
    // ID
    // - Numéro de chambre
    // - Type de chambre (simple, double, suite, etc.)
    // - Prix par nuit
    // - Capacité (nombre de personnes)
    // - Description de la chambre
    // - Disponibilité
    <form className="row g-3">
      <h4 className="text-center mb-4 fw-bold">
        Formulaire d'ajout d'une chambre
      </h4>
      <div className="col-md-6">
        <input
          type="text"
          className="form-control"
          name="noChambre"
          placeholder="No Chambre"
        />
      </div>
      <div className="col-md-6">
        <input
          type="text"
          className="form-control"
          name="typeChambre"
          placeholder="type de Chambre"
        />
      </div>
      <div className="col-md-6">
        <input
          type="text"
          className="form-control"
          name="nbrePers"
          placeholder="capacité de personne"
        />
      </div>
      <div className="col-md-6">
        <input
          type="text"
          className="form-control"
          name="prixCh"
          placeholder="prix de la Chambre"
        />
      </div>
      {/* <div className="col-12">
        <label htmlFor="inputAddress" className="form-label">
          Address
        </label>
        <input
          type="text"
          className="form-control"
          id="inputAddress"
          placeholder="1234 Main St"
        />
      </div> */}
      <div class="form-floating">
        <textarea
          class="form-control"
          placeholder="Description de la chambre"
          id="floatingTextarea"
        ></textarea>
        <label for="floatingTextarea">Description</label>
      </div>

      <div className="col-md-4">
        <label htmlFor="inputState" className="form-label">
          Disponibilité
        </label>
         <select id="inputState" className="form-select">
          <option selected>Disponible</option>
          <option>Occupé</option>
          <option>Fermé</option>
          <option>Supprimé</option>
        </select> 
      </div>

      
      <div className="col-12">
        <Link type="submit" className="btn btn-primary">
          Ajouter
        </Link>
      </div>
    </form>
  );
};

export default AjoutForm;
