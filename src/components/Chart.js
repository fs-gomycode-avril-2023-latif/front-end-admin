import React from 'react'

import { Column } from "@ant-design/plots";

const Chart = () => {
    const data = [
        {
          type: "Janvier",
          sales: 38,
        },
        {
          type: "Fevrier",
          sales: 52,
        },
        {
          type: "Mars",
          sales: 61,
        },
        {
          type: "Avril",
          sales: 145,
        },
        {
          type: "Mai",
          sales: 48,
        },
        {
          type: "Juin",
          sales: 38,
        },
        {
          type: "Juillet",
          sales: 38,
        },
        {
          type: "Aout",
          sales: 38,
        },
        {
          type: "Septembre",
          sales: 250,
        },
        {
          type: "Octobre",
          sales: 50,
        },
        {
          type: "Novembre",
          sales: 70,
        },
        {
          type: "Decembre",
          sales: 30,
        },
      ];
    
      const config = {
        data,
        xField: "type",
        yField: "sales",
        // couleur des bandes
        // color: ({ type }) => {
        //   return "#ffd333";
        // },
        label: {
          // 可手动配置 label 数据标签位置
          position: "middle",
          // 'top', 'bottom', 'middle',
          // 配置样式
          style: {
            fill: "#FFFFFF",
            opacity: 1,
          },
        },
        xAxis: {
          label: {
            autoHide: true,
            autoRotate: false,
          },
        },
        meta: {
          type: {
            alias: "Month",
          },
          sales: {
            alias: "Ventes",
          },
        },
      };
  return (
    <Column {...config} />
  )
}

export default Chart