import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import "./pages/login.css";
import Login from "./pages/Login";
import ForgotPassword from "./pages/ForgotPassword";
import ResetPassword from "./pages/ResetPassword";
import MainLayout from "./components/MainLayout";
import Dashboard from "./pages/Dashboard";
import Profile from "./pages/Profile";
import Client from "./pages/Client";
import Reservation from "./pages/Reservation";

import Facture from "./pages/Facture";
import Personnel from "./pages/personnel/Personnel.jsx";
import Commentaire from "./pages/Commentaire";
import TypeChambre from "./pages/TypeChambre";
import Error from "./pages/Error";
import Brand from "./pages/brand/Brand.jsx";
import AddBrand from "./pages/brand/AddBrand";
import Category from "./pages/category/Category.jsx";
import AddCategory from "./pages/category/AddCategory.jsx";

import Room from "./pages/room/Room";
import AddRoom from "./pages/room/AddRoom";
import Coupon from "./pages/coupon/Coupon";
import AddCoupon from "./pages/coupon/AddCoupon";
import Service from "./pages/service/Service";
import AddService from "./pages/service/AddService";
import AddPersonnel from "./pages/personnel/AddPersonnel";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="*" element={<Error />} />
          <Route path="/" element={<Login />} />
          <Route path="/forgot-password" element={<ForgotPassword />} />
          <Route path="/reset-password" element={<ResetPassword />} />
          <Route path="/admin" element={<MainLayout />}>
            <Route index element={<Dashboard />} />
            <Route path="profile" element={<Profile />} />

            <Route path="clients" element={<Client />} />
            {/* marques */}
            <Route path="listBrand" element={<Brand />} />
            <Route path="add-brand" element={<AddBrand />} />
            {/* chambres */}
            <Route path="list-room" element={<Room />} />
            <Route path="add-room" element={<AddRoom />} />
            {/* category */}
            <Route path="list-categorys" element={<Category />} />
            <Route path="add-category" element={<AddCategory />} />
            <Route path="reservation" element={<Reservation />} />
            {/* Service */}
            <Route path="list-service" element={<Service/>} />
            <Route path="add-service" element={<AddService />} />
            {/* Coupon */}
            <Route path="list-coupon" element={<Coupon />} />
            <Route path="add-coupon" element={<AddCoupon />} />
            <Route path="facture" element={<Facture />} />
            {/* personnel */}
            <Route path="list-personnel" element={<Personnel />} />
            <Route path="add-personnel" element={<AddPersonnel />} />
            <Route path="commentaire" element={<Commentaire />} />
            <Route path="type-chambre" element={<TypeChambre />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
